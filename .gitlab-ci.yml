stages:
  - build
  - test
  - publish
  - deploy

variables:
  SONAR_FLAGS: '/d:sonar.exclusions="tests/**/SampleFiles/*"'

include:
  - template: Security/SAST.gitlab-ci.yml
  - project: pommalabs/templates
    file: /.gitlab/ci_templates/dotnet/git-version.gitlab-ci.yml
  - project: pommalabs/templates
    file: /.gitlab/ci_templates/dotnet/.run-tests.gitlab-ci.yml
  - project: pommalabs/templates
    file: /.gitlab/ci_templates/dotnet/publish-sonar-report.gitlab-ci.yml
  - project: pommalabs/templates
    file: /.gitlab/ci_templates/docker/.publish-on-docker-hub.gitlab-ci.yml
  - project: pommalabs/templates
    file: /.gitlab/ci_templates/dotnet/publish-on-myget.gitlab-ci.yml
  - project: pommalabs/templates
    file: /.gitlab/ci_templates/dotnet/deploy-doxygen-docs.gitlab-ci.yml

.run-integration-tests:
  extends: .run-tests
  image: $CI_REGISTRY_IMAGE:sdk
  variables:
    FF_NETWORK_PER_BUILD: "true"
    # Tests are run as root, which requires disabling QT Web Engine sandbox in order to run Calibre
    # (see https://doc.qt.io/qt-5/qtwebengine-platform-notes.html#sandboxing-support):
    QTWEBENGINE_DISABLE_SANDBOX: "1"
    # Integration tests environment variables:
    SQL_SCRIPTS_PATH: "./tests/PommaLabs.Thumbnailer.IntegrationTests/SqlScripts"
    THUMBNAILER_RANDOM_SAMPLE: "true"
    Website__Enabled: "true"

run-integration-tests-without-db-anonymous:
  extends: .run-integration-tests
  variables:
    # Thumbnailer environment variables:
    Security__AllowAnonymousAccess: "true"

run-integration-tests-without-db:
  extends: .run-integration-tests
  variables:
    # Thumbnailer environment variables:
    Security__ApiKeys__0__Name: "INTEGRATION TESTS"
    Security__ApiKeys__0__Value: "__INTEGRATION_TESTS__"
    # Integration tests environment variables:
    THUMBNAILER_API_KEY: "__INTEGRATION_TESTS__"

run-integration-tests-with-mariadb:
  extends: .run-integration-tests
  services:
    - mariadb:latest
  variables:
    # MySQL environment variables (https://hub.docker.com/r/_/mariadb/):
    MYSQL_RANDOM_ROOT_PASSWORD: "yes"
    MYSQL_DATABASE: "thumbnailer"
    MYSQL_USER: "thumbnailer"
    MYSQL_PASSWORD: "thumbnailer"
    # Thumbnailer environment variables:
    Database__ConnectionString: "Server=mariadb;Database=thumbnailer;Uid=thumbnailer;Pwd=thumbnailer;"
    Database__Provider: "MySql"
    # Integration tests environment variables:
    THUMBNAILER_API_KEY: "__INTEGRATION_TESTS__"
  before_script:
    - mysql --host=mariadb --user=$MYSQL_USER --password=$MYSQL_PASSWORD --database=$MYSQL_DATABASE < $SQL_SCRIPTS_PATH/mysql.sql

run-integration-tests-with-postgresql:
  extends: .run-integration-tests
  services:
    - postgres:latest
  variables:
    # PostgreSQL environment variables (https://hub.docker.com/r/_/postgres/):
    POSTGRES_DB: "thumbnailer"
    POSTGRES_USER: "thumbnailer"
    POSTGRES_PASSWORD: "thumbnailer"
    # Thumbnailer environment variables:
    Database__ConnectionString: "Server=postgres;Port=5432;Database=thumbnailer;User Id=thumbnailer;Password=thumbnailer;"
    Database__Provider: "PostgreSql"
    # Integration tests environment variables:
    THUMBNAILER_API_KEY: "__INTEGRATION_TESTS__"
  before_script:
    - PGPASSWORD=$POSTGRES_PASSWORD psql --host=postgres --username=$POSTGRES_USER --dbname=$POSTGRES_DB --file=$SQL_SCRIPTS_PATH/postgresql.sql

run-integration-tests-with-sqlserver:
  extends: .run-integration-tests
  services:
    - name: mcr.microsoft.com/mssql/server:latest
      alias: mssql
  variables:
    # SQL Server environment variables (https://hub.docker.com/r/microsoft/mssql-server-linux/):
    ACCEPT_EULA: "Y"
    SA_PASSWORD: "thumbnailer(3!3)THUMBNAILER"
    MSSQL_PID: "Developer"
    # Thumbnailer environment variables:
    Database__ConnectionString: "Server=mssql;Database=thumbnailer;User Id=sa;Password=thumbnailer(3!3)THUMBNAILER;Encrypt=false;"
    Database__Provider: "SqlServer"
    # Integration tests environment variables:
    THUMBNAILER_API_KEY: "__INTEGRATION_TESTS__"
  before_script:
    - sqlcmd -S mssql -U sa -P "$SA_PASSWORD" -i $SQL_SCRIPTS_PATH/sqlserver.sql

publish-preview-on-docker-hub:
  extends: .publish-on-docker-hub-multi-arch
  variables:
    DOCKER_TAG: "$DOCKER_ORGANIZATION/$DOCKER_REPOSITORY:preview"
    DOCKERFILE_LOCATION: "src/PommaLabs.Thumbnailer/Dockerfile.latest"
  only:
    - preview

publish-latest-on-docker-hub:
  extends: .publish-on-docker-hub-multi-arch
  variables:
    DOCKER_TAG: "$DOCKER_ORGANIZATION/$DOCKER_REPOSITORY:latest"
    DOCKERFILE_LOCATION: "src/PommaLabs.Thumbnailer/Dockerfile.latest"
  only:
    - main

publish-azure-appsvc-on-docker-hub:
  extends: .publish-on-docker-hub
  variables:
    DOCKER_TAG: "$DOCKER_ORGANIZATION/$DOCKER_REPOSITORY:azure-appsvc"
    DOCKERFILE_LOCATION: "src/PommaLabs.Thumbnailer/Dockerfile.azure-appsvc"
  only:
    - main

deploy-latest-on-heroku:
  stage: deploy
  image: docker:latest
  services:
    - docker:dind
  variables:
    DOCKER_TAG: "$DOCKER_ORGANIZATION/$DOCKER_REPOSITORY:latest"
    HEROKU_REGISTRY: "registry.heroku.com"
  script:
    - docker login -u _ -p $HEROKU_API_KEY $HEROKU_REGISTRY
    - docker pull $DOCKER_TAG
    - docker tag $DOCKER_TAG $HEROKU_REGISTRY/$HEROKU_APP_NAME/web
    - docker push $HEROKU_REGISTRY/$HEROKU_APP_NAME/web
    - apk add --no-cache curl
    - |-
      curl -X PATCH https://api.heroku.com/apps/$HEROKU_APP_NAME/formation -H "Content-Type: application/json" -H "Accept: application/vnd.heroku+json; version=3.docker-releases" -H "Authorization: Bearer $HEROKU_API_KEY" -d '{ "updates": [ { "type": "web", "docker_image": "'$(docker inspect $HEROKU_REGISTRY/$HEROKU_APP_NAME/web --format={{.Id}})'" } ] }'
  only:
    - main
