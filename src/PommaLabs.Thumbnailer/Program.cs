﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer;

using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using PommaLabs.Thumbnailer.Core;
using Serilog;
using Serilog.Events;

/// <summary>
///   Represents the current application.
/// </summary>
public static class Program
{
    /// <summary>
    ///   The main entry point to the application.
    /// </summary>
    /// <param name="args">The arguments provided at start-up, if any.</param>
    public static int Main(string[] args)
    {
        var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? string.Empty;
        var isDevelopment = environment == Environments.Development;

        var loggerConfiguration = new LoggerConfiguration()
            .MinimumLevel.Is(isDevelopment ? LogEventLevel.Debug : LogEventLevel.Information)
            // Reduce log messages produced by system libraries.
            .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
            .MinimumLevel.Override("Microsoft.AspNetCore.Diagnostics.ExceptionHandlerMiddleware", LogEventLevel.Fatal)
            // Exclude health checks, invoked a lot by load balancers.
            .Filter.ByExcluding($"RequestPath = '{Constants.HealthChecksEndpoint}' and StatusCode = 200")
            .Filter.ByExcluding($"RequestPath = '{Constants.VersionEndpoint}' and StatusCode = 200")
            .Enrich.FromLogContext()
            .WriteTo.Async(a => a.File("Data/Logs/.log", rollingInterval: RollingInterval.Day))
            .WriteTo.Async(a => a.Console());

        var appInsightsInstrumentationKey = Environment.GetEnvironmentVariable("APPINSIGHTS_INSTRUMENTATIONKEY");
        if (!string.IsNullOrWhiteSpace(appInsightsInstrumentationKey))
        {
            loggerConfiguration = loggerConfiguration
                .WriteTo.ApplicationInsights(appInsightsInstrumentationKey, TelemetryConverter.Traces);
        }

        Log.Logger = loggerConfiguration.CreateLogger();

        try
        {
            Log.Information("Starting up");
            CreateHostBuilder(args).Build().Run();
            return 0;
        }
        catch (Exception ex)
        {
            Log.Fatal(ex, "Application start-up failed");
            return 1;
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }

    /// <summary>
    ///   Builds a new host for the application.
    /// </summary>
    /// <param name="args">The command-line arguments, if any.</param>
    /// <returns>A new <see cref="IHostBuilder">host builder</see>.</returns>
    private static IHostBuilder CreateHostBuilder(string[] args)
    {
        var port = Environment.GetEnvironmentVariable("PORT");
        Constants.Port = string.IsNullOrWhiteSpace(port) ? Constants.Port : port;

        return Host
            .CreateDefaultBuilder(args)
            .UseSerilog()
            .ConfigureWebHostDefaults(webBuilder => webBuilder
                .ConfigureKestrel(options => options.AddServerHeader = false)
                .UseStartup<Startup>()
                .UseUrls("http://*:" + Constants.Port));
    }
}
