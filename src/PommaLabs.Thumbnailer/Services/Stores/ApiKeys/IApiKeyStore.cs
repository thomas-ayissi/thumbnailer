﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.ApiKeys;

using System.Threading;
using System.Threading.Tasks;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   API key store.
/// </summary>
public interface IApiKeyStore
{
    /// <summary>
    ///   Creates a new temporary API key.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A new temporary API key.</returns>
    Task<ApiKeyCredentials> AddTempApiKeyAsync(CancellationToken cancellationToken);

    /// <summary>
    ///   Deletes all temporary API keys which have expired.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The number of API keys deleted.</returns>
    Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken);

    /// <summary>
    ///   Tries to validate given API key.
    /// </summary>
    /// <param name="apiKey">API key to be validated.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>True if given <paramref name="apiKey"/> has been validated, false otherwise.</returns>
    Task<ApiKeyCredentials?> ValidateApiKeyAsync(string apiKey, CancellationToken cancellationToken);
}
