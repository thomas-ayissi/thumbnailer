﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.ApiKeys;

using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   API key store which applies caching policies.
/// </summary>
public sealed class CachingApiKeyStore : IApiKeyStore
{
    private readonly IApiKeyStore _apiKeyStore;
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="apiKeyStore">API key store.</param>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    public CachingApiKeyStore(
        IApiKeyStore apiKeyStore,
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration)
    {
        _apiKeyStore = apiKeyStore;
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
    }

    /// <inheritdoc/>
    public async Task<ApiKeyCredentials> AddTempApiKeyAsync(CancellationToken cancellationToken)
    {
        var result = await _apiKeyStore.AddTempApiKeyAsync(cancellationToken);
        await _cache.ClearAsync(nameof(IApiKeyStore), cancellationToken);
        return result;
    }

    /// <inheritdoc/>
    public async Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken)
    {
        var result = await _apiKeyStore.DeleteExpiredTempApiKeysAsync(cancellationToken);
        await _cache.ClearAsync(nameof(IApiKeyStore), cancellationToken);
        return result;
    }

    /// <inheritdoc/>
    public async Task<ApiKeyCredentials?> ValidateApiKeyAsync(string apiKey, CancellationToken cancellationToken)
    {
        return await _cache.GetOrAddSlidingAsync(
            partition: nameof(IApiKeyStore),
            valueExpression: () => _apiKeyStore.ValidateApiKeyAsync(apiKey, cancellationToken),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);
    }
}
