﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Download;

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Download manager which applies validations.
/// </summary>
public sealed class ValidatingDownloadManager : IDownloadManager
{
    private readonly IDownloadManager _downloadManager;
    private readonly Validator _validator;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="downloadManager">Download manager.</param>
    /// <param name="logger">Logger.</param>
    public ValidatingDownloadManager(
        IDownloadManager downloadManager,
        ILogger<ValidatingDownloadManager> logger)
    {
        _downloadManager = downloadManager;
        _validator = new Validator(logger);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> DownloadFileAsync(Uri fileUri, CancellationToken cancellationToken)
    {
        // Preconditions
        _validator.ValidateFileUri(fileUri);

        var result = await _downloadManager.DownloadFileAsync(fileUri, cancellationToken);

        // Postconditions
        Trace.Assert(File.Exists(result.Path), "Downloaded file does not exist on file system");
        return result;
    }
}
