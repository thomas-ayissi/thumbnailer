﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Optimization;

using System.Threading;
using System.Threading.Tasks;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Thumbnail manager which wraps all operations into activities.
/// </summary>
public sealed class TracingOptimizationManager : IOptimizationManager
{
    private readonly IOptimizationManager _optimizationManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="optimizationManager">Optimization manager.</param>
    public TracingOptimizationManager(IOptimizationManager optimizationManager)
    {
        _optimizationManager = optimizationManager;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken)
    {
        using var activity = ThumbnailerActivitySource.Instance.StartActivity(Constants.ActivityNames.MediaOptimization);

        if (activity != null)
        {
            activity.AddTag(Constants.TagNames.CommandInternalJobId, command.InternalJobId);
            activity.AddTag(Constants.TagNames.CommandFileContentType, command.File.ContentType);
        }

        return await _optimizationManager.OptimizeMediaAsync(command, cancellationToken);
    }
}
