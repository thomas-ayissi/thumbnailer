﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Configurations;

using System;
using PommaLabs.Thumbnailer.Models.Enumerations;

/// <summary>
///   Database configuration.
/// </summary>
public sealed class DatabaseConfiguration
{
    /// <summary>
    ///   Backing field of <see cref="ConnectionString"/>.
    /// </summary>
    private string? _connectionString;

    /// <summary>
    ///   Backing field of <see cref="SchemaName"/>.
    /// </summary>
    private string? _schemaName;

    /// <summary>
    ///   How long each cache entry will live. Defaults to 20 minutes and should be greater than
    ///   <see cref="SecurityConfiguration.AsyncProcessTimeout"/>, since cache is used to track
    ///   asynchronous jobs completion.
    /// </summary>
    public TimeSpan CacheLifetime { get; set; } = TimeSpan.FromMinutes(20);

    /// <summary>
    ///   Connection string of a SQL database.
    /// </summary>
    public string ConnectionString
    {
        get => _connectionString ?? ParsePostgreSqlDatabaseUrl() ?? string.Empty;
        set => _connectionString = value;
    }

    /// <summary>
    ///   Provider.
    /// </summary>
    public DatabaseProvider Provider { get; set; }

    /// <summary>
    ///   The schema which holds Thumbnailer tables.
    /// </summary>
    public string SchemaName
    {
        get => string.IsNullOrWhiteSpace(_schemaName) ? (Provider switch { DatabaseProvider.PostgreSql => "public", DatabaseProvider.SqlServer => "dbo", _ => string.Empty }) : string.Empty;
        set => _schemaName = value;
    }

    internal string SchemaNameWithDot
    {
        get
        {
            var schemaName = SchemaName;
            return string.IsNullOrWhiteSpace(schemaName) ? string.Empty : $"{SchemaName}.";
        }
    }

    private static string? ParsePostgreSqlDatabaseUrl()
    {
        var databaseUrl = Environment.GetEnvironmentVariable("DATABASE_URL");

        if (string.IsNullOrWhiteSpace(databaseUrl))
        {
            return null;
        }

        var databaseUri = new Uri(databaseUrl);
        var userInfo = databaseUri.UserInfo.Split(':');

        var builder = new Npgsql.NpgsqlConnectionStringBuilder
        {
            Host = databaseUri.Host,
            Port = databaseUri.Port,
            Username = userInfo[0],
            Password = userInfo[1],
            Database = databaseUri.LocalPath.TrimStart('/'),
            SslMode = Npgsql.SslMode.Prefer,
            TrustServerCertificate = true,
        };

        return builder.ToString();
    }
}
