﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.DTO.Internal;

using System;
using System.IO;
using PommaLabs.KVLite;
using PommaLabs.MimeTypes;

/// <summary>
///   Represents a temporary file which is going to be processed or has been processed.
/// </summary>
public sealed class TempFileMetadata : IObjectWithCacheKey
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    public TempFileMetadata(string contentType)
        : this(contentType, null)
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="extension">File extension.</param>
    public TempFileMetadata(string contentType, string? extension)
    {
        ContentType = contentType;
        Extension = extension ?? MimeTypeMap.GetExtension(contentType);
    }

    /// <summary>
    ///   File content type (e.g. "image/png").
    /// </summary>
    public string ContentType { get; }

    /// <summary>
    ///   File extension, with leading dot (e.g. ".png").
    /// </summary>
    public string Extension { get; }

    /// <summary>
    ///   Checks whether this file represents an Adobe document (PDF or EPS).
    /// </summary>
    /// <returns>True if this file represents an Adobe document, false otherwise.</returns>
    public bool IsAdobeDocument => Extension switch
    {
        MimeTypeMap.Extensions.EPS => true,
        MimeTypeMap.Extensions.PDF => true,
        _ => false
    };

    /// <summary>
    ///   Checks whether this file encoding is binary.
    /// </summary>
    /// <returns>True if this file encoding is binary, false otherwise.</returns>
    public bool IsBinary => MimeTypeMap.TryGetEncoding(ContentType, out var encoding)
        ? encoding == MimeEncoding.Base64
        : !ContentType.StartsWith("text/", StringComparison.OrdinalIgnoreCase);

    /// <summary>
    ///   Checks whether this file represents an ebook.
    /// </summary>
    /// <returns>True if this file represents an ebook, false otherwise.</returns>
    public bool IsEbook => ContentType switch
    {
        MimeTypeMap.APPLICATION.EPUB_ZIP => true,
        _ => false
    };

    /// <summary>
    ///   Checks whether this file represents an HTML document.
    /// </summary>
    /// <returns>True if this file represents an HTML document, false otherwise.</returns>
    public bool IsHtmlDocument => Extension switch
    {
        MimeTypeMap.Extensions.HTML => true,
        MimeTypeMap.Extensions.XHTML => true,
        _ => false
    };

    /// <summary>
    ///   Checks whether this file represents a Microsoft Office or LibreOffice document.
    /// </summary>
    /// <returns>
    ///   True if this file represents a Microsoft Office or LibreOffice document, false otherwise.
    /// </returns>
    public bool IsOfficeDocument => Extension switch
    {
        // Microsoft Office:
        MimeTypeMap.Extensions.DOC => true,
        MimeTypeMap.Extensions.DOCM => true,
        MimeTypeMap.Extensions.DOCX => true,
        MimeTypeMap.Extensions.PPT => true,
        MimeTypeMap.Extensions.PPTM => true,
        MimeTypeMap.Extensions.PPTX => true,
        MimeTypeMap.Extensions.VSD => true,
        MimeTypeMap.Extensions.XLS => true,
        MimeTypeMap.Extensions.XLSM => true,
        MimeTypeMap.Extensions.XLSX => true,
        // LibreOffice:
        MimeTypeMap.Extensions.ODF => true,
        MimeTypeMap.Extensions.ODG => true,
        MimeTypeMap.Extensions.ODP => true,
        MimeTypeMap.Extensions.ODS => true,
        MimeTypeMap.Extensions.ODT => true,
        // Other formats:
        MimeTypeMap.Extensions.CSV => true,
        MimeTypeMap.Extensions.RTF => true,
        _ => false
    };

    /// <summary>
    ///   Checks whether this file represents an SVG document.
    /// </summary>
    /// <returns>True if this file represents an SVG document, false otherwise.</returns>
    public bool IsSvgDocument => ContentType switch
    {
        MimeTypeMap.IMAGE.SVG_XML => true,
        _ => false
    };

    /// <summary>
    ///   Checks whether this file represents a text document.
    /// </summary>
    /// <returns>True if this file represents a text document, false otherwise.</returns>
    public bool IsTextDocument => !IsAdobeDocument && !IsBinary && !IsHtmlDocument && !IsOfficeDocument && !IsSvgDocument;

    /// <summary>
    ///   Checks whether this file represents a video.
    /// </summary>
    /// <returns>True if this file represents a video, false otherwise.</returns>
    public bool IsVideo => Extension switch
    {
        MimeTypeMap.Extensions._3GP => true,
        MimeTypeMap.Extensions.AVI => true,
        MimeTypeMap.Extensions.FLV => true,
        MimeTypeMap.Extensions.MKV => true,
        MimeTypeMap.Extensions.MOV => true,
        MimeTypeMap.Extensions.MP4 => true,
        MimeTypeMap.Extensions.WEBM => true,
        MimeTypeMap.Extensions.WMV => true,
        _ => false
    };

    /// <summary>
    ///   File path on local file system.
    /// </summary>
    public string Path { get; set; } = string.Empty;

    /// <summary>
    ///   File size in bytes.
    /// </summary>
    public long Size => string.IsNullOrWhiteSpace(Path) ? 0L : new FileInfo(Path).Length;

    /// <inheritdoc/>
    public CacheKey GetCacheKey() => Path;
}
