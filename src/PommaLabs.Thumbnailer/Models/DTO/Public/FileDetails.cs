﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.DTO.Public;

using System;

/// <summary>
///   Represents a file which will be exchanged via API.
/// </summary>
public sealed class FileDetails
{
    /// <summary>
    ///   File contents, which the client will send/receive as a Base64 encoded string.
    /// </summary>
    public byte[] Contents { get; set; } = Array.Empty<byte>();

    /// <summary>
    ///   File content type (e.g. "image/png").
    /// </summary>
    public string ContentType { get; set; } = string.Empty;

    /// <summary>
    ///   File name (e.g. "image.png").
    /// </summary>
    public string FileName { get; set; } = string.Empty;
}
