﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Enumerations;

/// <summary>
///   Database provider list.
/// </summary>
public enum DatabaseProvider
{
    /// <summary>
    ///   No database will be used.
    /// </summary>
    None = 0,

    /// <summary>
    ///   PostgreSQL.
    /// </summary>
    PostgreSql = 1,

    /// <summary>
    ///   SQL Server.
    /// </summary>
    SqlServer = 2,

    /// <summary>
    ///   MySQL and MariaDB.
    /// </summary>
    MySql = 3
}
