﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Commands;

using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Represents all information required to perform a thumbnail generation command.
/// </summary>
public sealed class ThumbnailGenerationCommand : ThumbnailerCommandBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="file">File.</param>
    public ThumbnailGenerationCommand(TempFileMetadata file)
        : base(file)
    {
    }

    /// <summary>
    ///   <para>
    ///     If true, a transparent background will be added so that thumbnail width and height are
    ///     exactly as requested, preserving source file aspect ratio.
    ///   </para>
    ///   <para>When <see cref="SmartCrop"/> is enabled, this parameter will be ignored.</para>
    ///   <para>Defaults to true.</para>
    /// </summary>
    public bool Fill { get; init; } = true;

    /// <summary>
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <see cref="Fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </summary>
    public ushort HeightPx { get; init; } = Validator.ThumbnailSidePx;

    /// <summary>
    ///   Shaves specified pixels from source file edges.
    /// </summary>
    public ushort ShavePx { get; init; }

    /// <summary>
    ///   <para>
    ///     If true, finds a good crop which satisfies specified <see cref="WidthPx"/> and
    ///     <see cref="HeightPx"/> parameters.
    ///   </para>
    ///   <para>When smart crop is enabled, <see cref="Fill"/> is ignored.</para>
    ///   <para>Defaults to false.</para>
    /// </summary>
    public bool SmartCrop { get; init; }

    /// <summary>
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <see cref="Fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </summary>
    public ushort WidthPx { get; init; } = Validator.ThumbnailSidePx;

    /// <inheritdoc/>
    public override CacheKey GetCacheKey()
    {
        return new CacheKey(base.GetCacheKey(), WidthPx, HeightPx, ShavePx, Fill, SmartCrop);
    }

    /// <inheritdoc/>
    internal override void Validate(Validator validator)
    {
        validator.ValidateContentTypeForThumbnailGeneration(File.ContentType, @throw: true);
    }
}
