﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.V2.Controllers;

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.DTO.Public;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Services.Managers.Download;
using PommaLabs.Thumbnailer.Services.Managers.Job;
using PommaLabs.Thumbnailer.Services.Stores.TempFiles;

/// <summary>
///   Controller which handles thumbnail generation.
/// </summary>
[ApiController, Route("api/v{version:apiVersion}/thumbnail"), Authorize]
[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JobDetails))]
[ProducesResponseType(StatusCodes.Status400BadRequest)]
[ProducesResponseType(StatusCodes.Status401Unauthorized)]
[ProducesResponseType(StatusCodes.Status403Forbidden)]
public sealed class ThumbnailController : ControllerBase
{
    private readonly IDownloadManager _downloadManager;
    private readonly IJobManager _jobManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobManager">Job manager.</param>
    /// <param name="downloadManager">Download manager.</param>
    public ThumbnailController(
        IJobManager jobManager,
        IDownloadManager downloadManager)
    {
        _jobManager = jobManager;
        _downloadManager = downloadManager;
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When
    ///   <paramref name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and
    ///   <paramref name="heightPx"/> parameters. When smart crop is enabled,
    ///   <paramref name="fill"/> is ignored. Defaults to false.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    [HttpGet("")]
    public async Task<IActionResult> GenerateThumbnailAsync(
        [FromQuery] Uri fileUri,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await _downloadManager.DownloadFileAsync(fileUri, cancellationToken);
        var tgCommand = new ThumbnailGenerationCommand(localFile) { WidthPx = widthPx, HeightPx = heightPx, ShavePx = shavePx, Fill = fill, SmartCrop = smartCrop };
        return Ok(await _jobManager.EnqueueCommandAsync(tgCommand, cancellationToken));
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="tempFileStore">Temporary file store.</param>
    /// <param name="file">Source file.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When
    ///   <paramref name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and
    ///   <paramref name="heightPx"/> parameters. When smart crop is enabled,
    ///   <paramref name="fill"/> is ignored. Defaults to false.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    [HttpPost(""), Consumes(Constants.MultipartFormData)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> GenerateThumbnailAsync(
        [FromServices] ITempFileStore tempFileStore, IFormFile file,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileStore.HandleFileUploadAsync(file, cancellationToken);
        var tgCommand = new ThumbnailGenerationCommand(localFile) { WidthPx = widthPx, HeightPx = heightPx, ShavePx = shavePx, Fill = fill, SmartCrop = smartCrop };
        return Ok(await _jobManager.EnqueueCommandAsync(tgCommand, cancellationToken));
    }

    /// <summary>
    ///   Generates a thumbnail of given source file.
    /// </summary>
    /// <param name="tempFileStore">Temporary file store.</param>
    /// <param name="file">Source file.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When
    ///   <paramref name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and
    ///   <paramref name="heightPx"/> parameters. When smart crop is enabled,
    ///   <paramref name="fill"/> is ignored. Defaults to false.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    /// <remarks>Source file contents need to be encoded as Base64 string.</remarks>
    [HttpPost("base64"), Consumes(MimeTypeMap.APPLICATION.JSON, MimeTypeMap.APPLICATION.XML)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> GenerateThumbnailAsync(
        [FromServices] ITempFileStore tempFileStore,
        [FromBody] FileDetails file,
        [FromQuery] ushort widthPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort heightPx = Validator.ThumbnailSidePx,
        [FromQuery] ushort shavePx = Validator.ThumbnailShavePx,
        [FromQuery] bool fill = true,
        [FromQuery] bool smartCrop = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileStore.HandleFileUploadAsync(file, cancellationToken);
        var tgCommand = new ThumbnailGenerationCommand(localFile) { WidthPx = widthPx, HeightPx = heightPx, ShavePx = shavePx, Fill = fill, SmartCrop = smartCrop };
        return Ok(await _jobManager.EnqueueCommandAsync(tgCommand, cancellationToken));
    }
}
