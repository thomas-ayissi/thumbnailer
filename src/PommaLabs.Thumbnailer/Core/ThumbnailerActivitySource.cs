﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Core;

using System.Diagnostics;
using System.Reflection;

internal static class ThumbnailerActivitySource
{
    private static readonly AssemblyName s_assemblyName = typeof(ThumbnailerActivitySource).Assembly.GetName();

    internal static ActivitySource Instance { get; } = new(s_assemblyName.Name!, s_assemblyName.Version!.ToString());
}
