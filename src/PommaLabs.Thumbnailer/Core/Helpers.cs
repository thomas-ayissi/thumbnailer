﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Core;

internal static class Helpers
{
    public static string? CleanupFileName(string? fileName)
    {
        return fileName?.Replace("\"", string.Empty);
    }
}
