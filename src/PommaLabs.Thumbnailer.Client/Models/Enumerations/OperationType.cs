﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Models.Enumerations;

/// <summary>
///   Thumbnailer operation type.
/// </summary>
public enum OperationType
{
    /// <summary>
    ///   Thumbnail generation.
    /// </summary>
    ThumbnailGeneration = 0,

    /// <summary>
    ///   Media optimization.
    /// </summary>
    MediaOptimization = 1,

    /// <summary>
    ///   Async job retrieval.
    /// </summary>
    JobRetrieval = 2,
}
