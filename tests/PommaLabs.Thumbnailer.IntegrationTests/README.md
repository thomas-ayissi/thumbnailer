﻿# Thumbnailer integration tests

## Useful commands

To rename all "received" results into "verified" ones, overriding old ones:

```pwsh
ls *.* | Move-Item -Destination {$_.name -replace "received", "verified"} -Force
```