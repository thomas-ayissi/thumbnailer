CREATE DATABASE [thumbnailer]
GO

USE [thumbnailer]
GO

CREATE TABLE [dbo].[api_keys] (
    [id] BIGINT IDENTITY(1, 1) NOT NULL,
    [name] NVARCHAR(100) NOT NULL,
    [value] NVARCHAR(32) NOT NULL,
    [created_at] DATETIMEOFFSET(7) NOT NULL DEFAULT GETUTCDATE(),
    [expires_at] DATETIMEOFFSET(7) NULL DEFAULT NULL,
    [temporary] BIT NOT NULL DEFAULT 0,
    CONSTRAINT [pk_api_keys] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [ak_api_keys] UNIQUE ([value] ASC)
)
GO

INSERT INTO [dbo].[api_keys] ([name], [value]) VALUES ('INTEGRATION TESTS', '__INTEGRATION_TESTS__')
GO

CREATE TABLE [dbo].[cache_entries] (
    [kvle_id] BIGINT IDENTITY(1, 1) NOT NULL,
    [kvle_hash] BIGINT NOT NULL,
    [kvle_expiry] BIGINT NOT NULL,
    [kvle_interval] BIGINT NOT NULL,
    [kvle_value] VARBINARY(MAX) NOT NULL,
    [kvle_compressed] BIT NOT NULL,
    [kvle_partition] NVARCHAR(2000) NOT NULL,
    [kvle_key] NVARCHAR(2000) NOT NULL,
    [kvle_creation] BIGINT NOT NULL,
    [kvle_parent_hash0] BIGINT NULL DEFAULT NULL,
    [kvle_parent_key0] NVARCHAR(2000) NULL DEFAULT NULL,
    [kvle_parent_hash1] BIGINT NULL DEFAULT NULL,
    [kvle_parent_key1] NVARCHAR(2000) NULL DEFAULT NULL,
    [kvle_parent_hash2] BIGINT NULL DEFAULT NULL,
    [kvle_parent_key2] NVARCHAR(2000) NULL DEFAULT NULL,
    CONSTRAINT [pk_cache_entries] PRIMARY KEY CLUSTERED ([kvle_id] ASC),
    CONSTRAINT [uk_cache_entries] UNIQUE ([kvle_hash] ASC)
)
GO
